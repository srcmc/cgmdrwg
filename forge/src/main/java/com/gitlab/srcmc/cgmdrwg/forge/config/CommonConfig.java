/*
 * This file is part of Don't run with Guns.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Don't run with Guns is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Don't run with Guns is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Don't run with Guns. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.cgmdrwg.forge.config;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.gitlab.srcmc.cgmdrwg.ModCommon;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
import net.minecraftforge.registries.ForgeRegistries;

public class CommonConfig {
    private final ConfigValue<List<? extends String>> heavyItemsValue;
    private final ForgeConfigSpec spec;

    private final Set<Item> heavyItems = new HashSet<>();

    public CommonConfig() {
        this(new ForgeConfigSpec.Builder());
    }

    public CommonConfig(ForgeConfigSpec.Builder builder) {
        heavyItemsValue = builder
            .comment("# Comma seperated list of items that will prevent the player from sprinting",
                " while held in the main- or offhand. The format is: [\"<modid>:<item>\", ...].",
                "Default: [\"cgm:mini_gun\", \"cgm:bazooka\"]")
            .defineList("heavy_items", List.of("cgm:mini_gun", "cgm:bazooka"), itemId -> true);

        spec = builder.build();
    }

    public ForgeConfigSpec getSpec() {
        return spec;
    }

    public void init() {
        heavyItems.clear();

        for(var itemId : heavyItemsValue.get()) {
            var item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemId));
    
            if(item != Items.AIR || itemId.equals("minecraft:air")) {
                heavyItems.add(item);
            } else {
                ModCommon.LOG.warn(String.format("Item not found '%s'", itemId));
            }
        }
    }

    public Set<Item> getHeavyItems() {
        return heavyItems;
    }
}
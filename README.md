# Don't run with Guns

A small addon for [MrCrayfishs Gun Mod](https://www.curseforge.com/minecraft/mc-mods/mrcrayfishs-gun-mod) that prevents the player from sprinting while either aiming or shooting with a gun.

Additionally provides a config that allows the definition of so called *heavy items* that will prevent the player from sprinting aswell, while beeing held in either the main- or offhand. Includes the Minigun and Bazooka by default.

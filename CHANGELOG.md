# Changelog

## [1.0.2] - 2024-02-26

***Fixed***

- Missing version dependecies for Crayfishs Gun Mod and Framework

## [1.0.1] - 2024-02-10

***Removed***

- 'Configured' dependency

## [1.0.0] - 2024-02-10

***Added***

- Heavy items and sprint supression while holding them
- Sprint suppression while aiming

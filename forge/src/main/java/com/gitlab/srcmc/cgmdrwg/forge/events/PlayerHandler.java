/*
 * This file is part of Don't run with Guns.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Don't run with Guns is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Don't run with Guns is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Don't run with Guns. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.cgmdrwg.forge.events;

import com.gitlab.srcmc.cgmdrwg.ModCommon;
import com.gitlab.srcmc.cgmdrwg.forge.ModForge;
import com.mrcrayfish.guns.client.handler.AimingHandler;
import com.mrcrayfish.guns.event.GunFireEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@Mod.EventBusSubscriber(modid = ModCommon.MOD_ID, bus = Bus.FORGE)
public class PlayerHandler {
    @SubscribeEvent
    public static void onPlayerTick(TickEvent.PlayerTickEvent event) {
        var player = event.player;
        var items = new ItemStack[] {player.getMainHandItem(), player.getOffhandItem()};

        for(var itemInHand : items) {
            if(ModForge.COMMON_CONFIG.getHeavyItems().contains(itemInHand.getItem()) || AimingHandler.get().isAiming()) {
                stopSprinting(player);
                break;
            }
        }
    }

    @SubscribeEvent
    public static void onGunFired(GunFireEvent event) {
        stopSprinting(event.getEntity());
    }

    @SuppressWarnings("resource")
    private static void stopSprinting(Player player) {
        player.setSprinting(false);

        if(player.level.isClientSide) {
            Minecraft.getInstance().options.keySprint.setDown(false);
        }
    }
}
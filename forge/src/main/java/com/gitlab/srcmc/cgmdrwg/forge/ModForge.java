/*
 * This file is part of Don't run with Guns.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Don't run with Guns is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Don't run with Guns is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Don't run with Guns. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.cgmdrwg.forge;

import com.gitlab.srcmc.cgmdrwg.ModCommon;
import com.gitlab.srcmc.cgmdrwg.forge.config.CommonConfig;

import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.config.ModConfigEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(ModCommon.MOD_ID)
public class ModForge {
    public static CommonConfig COMMON_CONFIG;

    static { ModRegistries.init(); }

    public ModForge() {
        COMMON_CONFIG = new CommonConfig();
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, COMMON_CONFIG.getSpec());
		IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
    	bus.addListener(this::onConfigReload);
    }

    private void onConfigReload(final ModConfigEvent event) {
        COMMON_CONFIG.init();
    }
}